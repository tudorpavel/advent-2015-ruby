# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'
require 'set'

require_relative './house'

module Day03
  class Santa < T::Struct
    extend T::Sig

    prop :current_x, Integer, default: 0
    prop :current_y, Integer, default: 0
    prop :houses, T::Set[House]

    sig { params(houses: T::Set[House]).returns(Santa) }
    def self.build(houses)
      Santa.new(houses: houses)
    end

    sig { params(directions: T::Array[String]).void }
    def travel(directions)
      directions.each do |char|
        case char
        when '^' then @current_y += 1
        when 'v' then @current_y -= 1
        when '>' then @current_x += 1
        when '<' then @current_x -= 1
        else raise "Unexpected direction: #{char}"
        end

        @houses.add(House.new(x: @current_x, y: @current_y))
      end
    end
  end
end
