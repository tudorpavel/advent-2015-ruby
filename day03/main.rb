# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './santa_delivery'

lines = Utils::ParseInput.new.read_lines(ARGF.readlines)

directions = T.must(lines[0])

delivery = Day03::SantaDelivery.new
delivery.travel_alone(directions)
puts "Part1: #{delivery.unique_house_count}"

delivery = Day03::SantaDelivery.new
delivery.travel_with_robo_santa(directions)
puts "Part2: #{delivery.unique_house_count}"
