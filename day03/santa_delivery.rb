# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'
require 'set'

require_relative './santa'

module Day03
  class SantaDelivery < T::Struct
    extend T::Sig

    prop :houses, T::Set[House], default: Set[House.new(x: 0, y: 0)]

    sig { params(directions: String).void }
    def travel_alone(directions)
      santa = Santa.build(@houses)
      santa.travel(directions.chars)
    end

    sig { params(directions: String).void }
    def travel_with_robo_santa(directions)
      santa = Santa.build(@houses)
      robo_santa = Santa.build(@houses)

      santa.travel(
        directions.chars.select.with_index { |_, i| i.even? }
      )
      robo_santa.travel(
        directions.chars.select.with_index { |_, i| i.odd? }
      )
    end

    sig { returns(Integer) }
    def unique_house_count
      @houses.size
    end
  end
end
