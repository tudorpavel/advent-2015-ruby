# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day03
  class House < T::Struct
    extend T::Sig

    const :x, Integer
    const :y, Integer

    sig { params(other: House).returns(T::Boolean) }
    def eql?(other)
      @x == other.x && @y == other.y
    end

    sig { returns(Integer) }
    def hash
      @x + (2 * @y)
    end
  end
end
