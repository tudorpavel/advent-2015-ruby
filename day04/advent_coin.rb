# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'
require 'digest'

module Day04
  class AdventCoin < T::Struct
    extend T::Sig

    const :secret_key, String

    sig { params(leading_zero_count: Integer).returns(Integer) }
    def find_answer(leading_zero_count)
      (1..).each do |candidate|
        hash = Digest::MD5.hexdigest(@secret_key + candidate.to_s)
        return candidate if hash[..leading_zero_count - 1] == '0' * leading_zero_count
      end

      0
    end
  end
end
