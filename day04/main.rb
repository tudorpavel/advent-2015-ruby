# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './advent_coin'

lines = Utils::ParseInput.new.read_lines(ARGF.readlines)

secret_key = T.must(lines[0])

coin = Day04::AdventCoin.new(secret_key: secret_key)
puts "Part1: #{coin.find_answer(5)}"
puts "Part2: #{coin.find_answer(6)}"
