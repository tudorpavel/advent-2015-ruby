# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './building'

lines = Utils::ParseInput.new.read_lines(ARGF.readlines)

instructions = T.must(lines[0])
building = Day01::Building.new
puts "Part1: #{building.final_floor(instructions)}"
puts "Part2: #{building.basement_position(instructions)}"
