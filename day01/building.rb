# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day01
  class Building
    extend T::Sig

    sig { params(floor_instructions: String).returns(Integer) }
    def final_floor(floor_instructions)
      floor_instructions
        .chars
        .map { |c| c == '(' ? 1 : -1 }
        .reduce(:+)
    end

    sig { params(floor_instructions: String).returns(Integer) }
    def basement_position(floor_instructions)
      floor = 0

      floor_instructions
        .chars
        .map { |c| c == '(' ? 1 : -1 }
        .each_with_index do |delta, index|
          floor += delta
          return index + 1 if floor.negative?
        end

      0
    end
  end
end
