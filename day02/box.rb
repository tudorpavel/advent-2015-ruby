# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day02
  class Box < T::Struct
    extend T::Sig

    const :length, Integer
    const :width, Integer
    const :height, Integer

    sig { params(input: String).returns(Box) }
    def self.build(input)
      l, w, h = input.split('x').map(&:to_i)

      Box.new(
        length: l || 0,
        width: w || 0,
        height: h || 0
      )
    end

    sig { returns(Integer) }
    def surface_area
      (2 * @length * @width) +
        (2 * @width * @height) +
        (2 * @length * @height)
    end

    sig { returns(Integer) }
    def smallest_side_area
      sorted = [@length, @width, @height].sort

      T.must(sorted[0]) * T.must(sorted[1])
    end

    sig { returns(Integer) }
    def smallest_side_perimeter
      sorted = [@length, @width, @height].sort

      2 * (T.must(sorted[0]) + T.must(sorted[1]))
    end

    sig { returns(Integer) }
    def volume
      [@length, @width, @height].reduce(&:*)
    end
  end
end
