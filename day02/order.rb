# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative './box'

module Day02
  class Order < T::Struct
    extend T::Sig

    const :boxes, T::Array[Box]

    sig { params(box_inputs: T::Array[String]).returns(Order) }
    def self.build(box_inputs)
      boxes = box_inputs.map { |input| Box.build(input) }

      Order.new(boxes: boxes)
    end

    sig { returns(Integer) }
    def total_wrapping_paper
      @boxes.map { |box| box.surface_area + box.smallest_side_area }.reduce(&:+)
    end

    sig { returns(Integer) }
    def total_ribbon
      @boxes.map { |box| box.volume + box.smallest_side_perimeter }.reduce(&:+)
    end
  end
end
