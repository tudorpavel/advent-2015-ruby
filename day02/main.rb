# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './order'

box_inputs = Utils::ParseInput.new.read_lines(ARGF.readlines)

order = Day02::Order.build(box_inputs)
puts "Part1: #{order.total_wrapping_paper}"
puts "Part2: #{order.total_ribbon}"
