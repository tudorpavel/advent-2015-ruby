# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './check_list'

strings = Utils::ParseInput.new.read_lines(ARGF.readlines)

check_list = Day05::CheckList.new(strings: strings)
puts "Part1: #{check_list.nice_count}"
puts "Part2: #{check_list.nice_count_part2}"
