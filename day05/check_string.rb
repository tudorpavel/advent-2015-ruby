# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day05
  class CheckString
    extend T::Sig

    sig { params(string: String).returns(T::Boolean) }
    def nice?(string)
      return false if bad_words?(string)

      vowel_count(string) > 2 && double_letter?(string)
    end

    private

    sig { params(string: String).returns(T::Boolean) }
    def bad_words?(string)
      !(string =~ /ab|cd|pq|xy/).nil?
    end

    sig { params(string: String).returns(Integer) }
    def vowel_count(string)
      string.count('aeiou')
    end

    sig { params(string: String).returns(T::Boolean) }
    def double_letter?(string)
      string.chars.each_cons(2) { |(c1, c2)| return true if c1 == c2 }
      false
    end
  end
end
