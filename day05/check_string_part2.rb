# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day05
  class CheckStringPart2
    extend T::Sig

    sig { params(string: String).returns(T::Boolean) }
    def nice?(string)
      pair_appearing_twice_without_overlap?(string) && symmetric_triple?(string)
    end

    private

    sig { params(string: String).returns(T::Boolean) }
    def pair_appearing_twice_without_overlap?(string)
      pairs = {}
      string.chars.each_cons(2).with_index do |(c1, c2), idx|
        pair_str = "#{c1}#{c2}"

        # if a pair was seen but not at index-1 which would mean current
        # pair overlaps with it
        return true if !pairs[pair_str].nil? && pairs[pair_str] < idx - 1

        # save the index where we first saw a given pair
        pairs[pair_str] ||= idx
      end

      false
    end

    sig { params(string: String).returns(T::Boolean) }
    def symmetric_triple?(string)
      string.chars.each_cons(3) do |(c1, _, c3)|
        return true if c1 == c3
      end

      false
    end
  end
end
