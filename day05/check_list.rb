# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative './check_string'
require_relative './check_string_part2'

module Day05
  class CheckList < T::Struct
    extend T::Sig

    const :strings, T::Array[String]
    const :check_string, CheckString, default: CheckString.new
    const :check_string_part2, CheckStringPart2, default: CheckStringPart2.new

    sig { returns(Integer) }
    def nice_count
      @strings.count { |s| @check_string.nice?(s) }
    end

    sig { returns(Integer) }
    def nice_count_part2
      @strings.count { |s| @check_string_part2.nice?(s) }
    end
  end
end
