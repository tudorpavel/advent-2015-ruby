# typed: false
# frozen_string_literal: true

require_relative '../../day02/box'

RSpec.describe Day02::Box do
  describe '.build' do
    it 'builds a Box from the input string' do
      box = described_class.build('2x3x4')
      expect(box.serialize).to eq('length' => 2, 'width' => 3, 'height' => 4)
    end
  end

  describe '#surface_area' do
    it 'returns correct total area of the box for 2x3x4' do
      box = described_class.new(length: 2, width: 3, height: 4)
      expect(box.surface_area).to eq(52)
    end

    it 'returns correct total area of the box for 1x1x10' do
      box = described_class.new(length: 1, width: 1, height: 10)
      expect(box.surface_area).to eq(42)
    end
  end

  describe '#smallest_side_area' do
    it 'returns the area of the smallest side for 2x3x4' do
      box = described_class.new(length: 2, width: 3, height: 4)
      expect(box.smallest_side_area).to eq(6)
    end

    it 'returns the area of the smallest side for 1x1x10' do
      box = described_class.new(length: 1, width: 1, height: 10)
      expect(box.smallest_side_area).to eq(1)
    end
  end

  describe '#smallest_side_perimeter' do
    it 'returns the perimeter of the smallest side for 2x3x4' do
      box = described_class.new(length: 2, width: 3, height: 4)
      expect(box.smallest_side_perimeter).to eq(10)
    end

    it 'returns the perimeter of the smallest side for 1x1x10' do
      box = described_class.new(length: 1, width: 1, height: 10)
      expect(box.smallest_side_perimeter).to eq(4)
    end
  end

  describe '#volume' do
    it 'returns the perimeter of the smallest side for 2x3x4' do
      box = described_class.new(length: 2, width: 3, height: 4)
      expect(box.volume).to eq(24)
    end

    it 'returns the perimeter of the smallest side for 1x1x10' do
      box = described_class.new(length: 1, width: 1, height: 10)
      expect(box.volume).to eq(10)
    end
  end
end
