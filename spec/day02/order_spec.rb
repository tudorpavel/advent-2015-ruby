# typed: false
# frozen_string_literal: true

require_relative '../../day02/order'

RSpec.describe Day02::Order do
  describe '.build' do
    it 'builds an Order from an array of input strings' do
      order = described_class.build(%w[2x3x4 1x1x10])
      expect(order.serialize).to eq('boxes' => [{ 'length' => 2, 'width' => 3, 'height' => 4 },
                                                { 'length' => 1, 'width' => 1, 'height' => 10 }])
    end
  end

  describe '#total_wrapping_paper' do
    it 'returns the total square feet of wrapping paper needed for all boxes' do
      order = described_class.build(%w[2x3x4 1x1x10])
      expect(order.total_wrapping_paper).to eq(58 + 43)
    end
  end

  describe '#total_ribbon' do
    it 'returns the total feet of ribbon needed for all boxes' do
      order = described_class.build(%w[2x3x4 1x1x10])
      expect(order.total_ribbon).to eq(34 + 14)
    end
  end
end
