# typed: false
# frozen_string_literal: true

require_relative '../../day03/santa_delivery'

RSpec.describe Day03::SantaDelivery, :aggregate_failures do
  subject(:delivery) { described_class.new }

  describe '#travel_alone' do
    it 'keeps correct track of unique houses visited for example 1' do
      delivery.travel_alone('>')
      expect(delivery.unique_house_count).to eq(2)
    end

    it 'keeps correct track of unique houses visited for example 2' do
      delivery.travel_alone('^>v<')
      expect(delivery.unique_house_count).to eq(4)
    end

    it 'keeps correct track of unique houses visited for example 3' do
      delivery.travel_alone('^v^v^v^v^v')
      expect(delivery.unique_house_count).to eq(2)
    end
  end

  describe '#travel_with_robo_santa' do
    it 'keeps correct track of unique houses visited for example 1' do
      delivery.travel_with_robo_santa('^v')
      expect(delivery.unique_house_count).to eq(3)
    end

    it 'keeps correct track of unique houses visited for example 2' do
      delivery.travel_with_robo_santa('^>v<')
      expect(delivery.unique_house_count).to eq(3)
    end

    it 'keeps correct track of unique houses visited for example 3' do
      delivery.travel_with_robo_santa('^v^v^v^v^v')
      expect(delivery.unique_house_count).to eq(11)
    end
  end
end
