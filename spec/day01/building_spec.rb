# typed: false
# frozen_string_literal: true

require_relative '../../day01/building'

RSpec.describe Day01::Building do
  subject(:building) { described_class.new }

  describe '#final_floor' do
    it 'solves part 1 examples above basement', :aggregate_failures do
      expect(building.final_floor('(())')).to eq(0)
      expect(building.final_floor('()()')).to eq(0)
      expect(building.final_floor('(((')).to eq(3)
      expect(building.final_floor('(()(()(')).to eq(3)
      expect(building.final_floor('))(((((')).to eq(3)
    end

    it 'solves part 1 examples ending in the basement', :aggregate_failures do
      expect(building.final_floor('())')).to eq(-1)
      expect(building.final_floor('))(')).to eq(-1)
      expect(building.final_floor(')))')).to eq(-3)
      expect(building.final_floor(')())())')).to eq(-3)
    end
  end

  describe '#basement_position' do
    it 'solves part 2 examples', :aggregate_failures do
      expect(building.basement_position(')')).to eq(1)
      expect(building.basement_position('()())')).to eq(5)
    end
  end
end
