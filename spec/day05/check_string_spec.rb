# typed: false
# frozen_string_literal: true

require_relative '../../day05/check_string'

RSpec.describe Day05::CheckString do
  subject(:check_string) { described_class.new }

  describe '#nice?' do
    it 'returns correct answers for examples at part 1', :aggregate_failures do
      expect(check_string.nice?('ugknbfddgicrmopn')).to be(true)
      expect(check_string.nice?('aaa')).to be(true)
      expect(check_string.nice?('jchzalrnumimnmhp')).to be(false)
      expect(check_string.nice?('haegwjzuvuyypxyu')).to be(false)
      expect(check_string.nice?('dvszwmarrgswjxmb')).to be(false)
    end
  end
end
