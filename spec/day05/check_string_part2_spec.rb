# typed: false
# frozen_string_literal: true

require_relative '../../day05/check_string_part2'

RSpec.describe Day05::CheckStringPart2 do
  subject(:check_string) { described_class.new }

  describe '#nice?' do
    it 'returns correct answers for examples at part 2', :aggregate_failures do
      expect(check_string.nice?('qjhvhtzxzqqjkmpb')).to be(true)
      expect(check_string.nice?('xxyxx')).to be(true)
      expect(check_string.nice?('uurcxstgmygtbstg')).to be(false)
      expect(check_string.nice?('ieodomkazucvgmuy')).to be(false)
    end

    it 'returns correct answers for my examples', :aggregate_failures do
      expect(check_string.nice?('xyaaaxy')).to be(true)
      expect(check_string.nice?('aaa')).to be(false)
      expect(check_string.nice?('aaaaa')).to be(true)
    end
  end
end
