# typed: false
# frozen_string_literal: true

require_relative '../../day05/check_list'

RSpec.describe Day05::CheckList do
  describe '#nice_count' do
    it 'returns correct answer for examples at part 1' do
      check_list = described_class.new(strings: %w[ugknbfddgicrmopn aaa
                                                   jchzalrnumimnmhp
                                                   haegwjzuvuyypxyu
                                                   dvszwmarrgswjxmb])
      expect(check_list.nice_count).to eq(2)
    end
  end

  describe '#nice_count_part2' do
    it 'returns correct answer for examples at part 2' do
      check_list = described_class.new(strings: %w[qjhvhtzxzqqjkmpb
                                                   xxyxx
                                                   uurcxstgmygtbstg
                                                   ieodomkazucvgmuy])
      expect(check_list.nice_count_part2).to eq(2)
    end
  end
end
