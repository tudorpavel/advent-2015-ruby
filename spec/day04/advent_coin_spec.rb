# typed: false
# frozen_string_literal: true

require_relative '../../day04/advent_coin'

RSpec.describe Day04::AdventCoin do
  describe '#find_answer' do
    skip 'find correct answer for part 1 example 1' do
      coin = described_class.new(secret_key: 'abcdef')
      expect(coin.find_answer(5)).to eq(609_043)
    end

    skip 'find correct answer for part 1 example 2' do
      coin = described_class.new(secret_key: 'pqrstuv')
      expect(coin.find_answer(5)).to eq(1_048_970)
    end

    skip 'find correct answer for part 2 example 1' do
      coin = described_class.new(secret_key: 'abcdef')
      expect(coin.find_answer(6)).to eq(6_742_839)
    end
  end
end
