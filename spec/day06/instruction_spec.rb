# typed: false
# frozen_string_literal: true

require_relative '../../day06/instruction'

RSpec.describe Day06::Instruction do
  describe '.build' do
    it 'parses input lines correctly', :aggregate_failures do # rubocop:disable RSpec/ExampleLength
      instruction1 = described_class.build('turn on 0,1 through 899,999')
      expect(instruction1.action).to eq(Day06::Instruction::Action::TurnOn)
      expect(instruction1.start_x).to eq(0)
      expect(instruction1.end_x).to eq(899)
      expect(instruction1.start_y).to eq(1)
      expect(instruction1.end_y).to eq(999)

      instruction2 = described_class.build('toggle 0,0 through 999,0')
      expect(instruction2.action).to eq(Day06::Instruction::Action::Toggle)

      instruction3 = described_class.build('turn off 499,499 through 500,500')
      expect(instruction3.action).to eq(Day06::Instruction::Action::TurnOff)
    end
  end
end
