# typed: false
# frozen_string_literal: true

require_relative '../../day06/instruction'
require_relative '../../day06/lights_grid'

RSpec.describe Day06::LightsGrid do
  subject(:lights_grid) { described_class.build }

  describe '.build' do
    it 'initializes grid with 1 million lights that are Off', :aggregate_failures do
      expect(lights_grid.grid.size).to eq(1000)
      expect(lights_grid.grid[0].size).to eq(1000)
      expect(lights_grid.grid[500][500]).to eq(0)
    end
  end

  describe '#perform_instruction' do
    it 'turns on lights' do
      lights_grid.perform_instruction(Day06::Instruction.build('turn on 0,0 through 9,0'))

      expect(lights_grid.grid[0][0..9]).to eq([1] * 10)
    end

    it 'turns off lights', :aggregate_failures do
      lights_grid.perform_instruction(Day06::Instruction.build('turn on 0,0 through 9,9'))
      lights_grid.perform_instruction(Day06::Instruction.build('turn off 0,0 through 4,1'))

      expect(lights_grid.grid[0][0..4]).to eq([0] * 5)
      expect(lights_grid.grid[1][0..4]).to eq([0] * 5)
      expect(lights_grid.grid[0][5..9]).to eq([1] * 5)
    end

    it 'toggles lights', :aggregate_failures do
      lights_grid.perform_instruction(Day06::Instruction.build('turn on 0,0 through 4,0'))
      lights_grid.perform_instruction(Day06::Instruction.build('toggle 0,0 through 9,0'))

      expect(lights_grid.grid[0][0..4]).to eq([0] * 5)
      expect(lights_grid.grid[0][5..9]).to eq([1] * 5)
    end
  end

  describe '#perform_instruction_part2' do
    it 'increase brightness', :aggregate_failures do
      lights_grid.perform_instruction_part2(Day06::Instruction.build('turn on 0,0 through 9,0'))
      expect(lights_grid.grid[0][0..9]).to eq([1] * 10)

      lights_grid.perform_instruction_part2(Day06::Instruction.build('turn on 0,0 through 9,0'))
      expect(lights_grid.grid[0][0..9]).to eq([2] * 10)
    end

    it 'decrease brightness to a minimum of 0', :aggregate_failures do
      lights_grid.perform_instruction_part2(Day06::Instruction.build('turn off 0,0 through 9,0'))
      expect(lights_grid.grid[0][0..9]).to eq([0] * 10)

      lights_grid.perform_instruction_part2(Day06::Instruction.build('toggle 0,0 through 9,0'))
      lights_grid.perform_instruction_part2(Day06::Instruction.build('turn off 0,0 through 9,0'))
      expect(lights_grid.grid[0][0..9]).to eq([1] * 10)
    end

    it 'increase brightness twice' do
      lights_grid.perform_instruction_part2(Day06::Instruction.build('toggle 0,0 through 9,0'))
      expect(lights_grid.grid[0][0..9]).to eq([2] * 10)
    end
  end

  describe '#turned_on_count' do
    it 'returns the correct count' do
      lights_grid.perform_instruction(Day06::Instruction.build('turn on 0,0 through 9,9'))

      expect(lights_grid.turned_on_count).to eq(100)
    end
  end

  describe '#total_brightness' do
    it 'returns the correct count' do
      lights_grid.perform_instruction_part2(Day06::Instruction.build('toggle 0,0 through 9,9'))

      expect(lights_grid.total_brightness).to eq(200)
    end
  end
end
