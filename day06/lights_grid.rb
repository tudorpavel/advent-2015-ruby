# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day06
  class LightsGrid < T::Struct
    extend T::Sig

    const :grid, T::Array[T::Array[Integer]]

    sig { returns(LightsGrid) }
    def self.build
      grid = Array.new(1000) { Array.new(1000, 0) }

      LightsGrid.new(grid: grid)
    end

    sig { params(instruction: Instruction).void }
    def perform_instruction(instruction) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      (instruction.start_y..instruction.end_y).each do |y|
        (instruction.start_x..instruction.end_x).each do |x|
          action = instruction.action
          T.must(grid[y])[x] = case action
                               when Instruction::Action::TurnOn
                                 1
                               when Instruction::Action::TurnOff
                                 0
                               when Instruction::Action::Toggle
                                 T.must(T.must(grid[y])[x]).zero? ? 1 : 0
                               else T.absurd(action)
                               end
        end
      end
    end

    sig { params(instruction: Instruction).void }
    def perform_instruction_part2(instruction) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      (instruction.start_y..instruction.end_y).each do |y|
        (instruction.start_x..instruction.end_x).each do |x|
          action = instruction.action
          T.must(grid[y])[x] = case action
                               when Instruction::Action::TurnOn
                                 T.must(T.must(grid[y])[x]) + 1
                               when Instruction::Action::TurnOff
                                 T.must(T.must(grid[y])[x]).zero? ? 0 : T.must(T.must(grid[y])[x]) - 1
                               when Instruction::Action::Toggle
                                 T.must(T.must(grid[y])[x]) + 2
                               else T.absurd(action)
                               end
        end
      end
    end

    sig { returns(Integer) }
    def turned_on_count
      total_brightness
    end

    sig { returns(Integer) }
    def total_brightness
      grid.sum(&:sum)
    end
  end
end
