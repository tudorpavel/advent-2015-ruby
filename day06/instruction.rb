# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

module Day06
  class Instruction < T::Struct
    extend T::Sig

    class Action < T::Enum
      enums do
        TurnOn = new
        TurnOff = new
        Toggle = new
      end
    end

    const :action, Action
    const :start_x, Integer
    const :end_x, Integer
    const :start_y, Integer
    const :end_y, Integer

    sig { params(line: String).returns(Instruction) }
    def self.build(line) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      match = T.must(/(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)/.match(line))

      action = case match[1]
               when 'turn on' then Action::TurnOn
               when 'turn off' then Action::TurnOff
               when 'toggle' then Action::Toggle
               else
                 raise ArgumentError, "Unexpected action: #{match[1]}"
               end

      Instruction.new(action: action,
                      start_x: T.must(match[2]).to_i,
                      end_x: T.must(match[4]).to_i,
                      start_y: T.must(match[3]).to_i,
                      end_y: T.must(match[5]).to_i)
    end
  end
end
