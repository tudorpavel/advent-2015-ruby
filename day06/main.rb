# typed: strict
# frozen_string_literal: true

require 'sorbet-runtime'

require_relative '../utils/parse_input'
require_relative './instruction'
require_relative './lights_grid'

input_lines = Utils::ParseInput.new.read_lines(ARGF.readlines)

lights_grid = Day06::LightsGrid.build
input_lines.each do |line|
  lights_grid.perform_instruction(Day06::Instruction.build(line))
end

puts "Part1: #{lights_grid.turned_on_count}"

lights_grid = Day06::LightsGrid.build
input_lines.each do |line|
  lights_grid.perform_instruction_part2(Day06::Instruction.build(line))
end

puts "Part2: #{lights_grid.total_brightness}"
